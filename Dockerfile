FROM node:7.7.2-alpine
WORKDIR /usr/app
RUN npm install http-server -g --quiet
COPY . .
EXPOSE 8080